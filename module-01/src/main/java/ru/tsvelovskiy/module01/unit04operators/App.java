package ru.tsvelovskiy.module01.unit04operators;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Блок 4:
 * Операторы, ветвления, циклы и лейблы.
 */
public class App {

    public static void main(String[] args) {
//        ОПЕРАТОРЫ В JAVA:
//        1. Арифметические операторы.
//        2. Операторы сравнения.
//        3. Побитовые операторы.
//        4. Логические операторы.
//        5. Операторы присваивания.
//        6. Тернарный оператор.
        int param = 5;
        String string = (param == 0) ? "ААА" : "ВВВ";
//        7. Оператор instanceof.
//        Возвращает значение типа boolean.
//        Нужен для того, чтобы проверить:
//          а) Является ли класс объекта наследником определенного класса.
//          б) Реализует ли класс объекта определенный интерфейс.

//        (*) Интересные моменты:

        //Постфиксная операция (например, инкремента).
        int a = 5;
        System.out.println(a++);    //Результат операции будет 5.
        System.out.println(a);      //А вот здесь мы уже увидим, что a увеличилось на 1.

        //Префиксная операция (например, инкремента).
        int b = 5;
        System.out.println(++b);    //Результат операции будет 6.
        System.out.println(b);      //Здесь мы также увидим 6.

//        ВЕТВЛЕНИЯ:
//        1. Оператор if.
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        boolean result = x > 0;
//        if или else if принимает значение (либо выражение) типа boolean.
        if (result) {                               //if всегда только один в такой цепочке.
            System.out.println("Больше нуля");
        } else if (x == 0) {                        //else if может быть сколько угодно в такой цепочке.
            System.out.println("Равно нулю");
        } else {                                    //else всегда один в такой цепочке.
            System.out.println("Меньше нуля");
        }

        if (result) {
            System.out.println("Больше нуля");
        }
        if (!result) {
            System.out.println("Меньше или равно нуля");
        } else {                                    //else всегда относится к последнему if (else if).
            System.out.println("Больше нуля");
        }

//        2. Оператор switch
//        В операторе switch можно использовать: char, byte, short, int, их обертки, String, enum.
//        В качестве выражения для case можно использовать литералы, enum, final переменные.
        String str = scanner.next();
        switch (str) {
            case "один":
                System.out.println("1");
                break;
            case "два":
                System.out.println("2");
                break;
            case "три":
                System.out.println("3");
                break;
            default:
                System.out.println("Ничего не найдено");
                break;
        }

//        ЦИКЛЫ и ЛЕЙБЛЫ:
//        1. Виды циклов:
//           а) Цикл со счетчиком.
        for(int i = 0; i < 10; i++) {
            System.out.println(i);
        }
//           б) цикл for для коллекций (побегает по всем элементам коллекции).
        List<String> stringList = Arrays.asList("AAA", "BBB", "CCC");
        for (String s : stringList) {
            System.out.println(s);
        }
//           в) цикл с предусловием (может не выполниться ни 1 раза).
        int count1 = 0;
        while (count1 < 5) {
            System.out.println(count1);
            count1++;
        }
//           г) Цикл с постусловием (гарантированно выполняется хотя бы 1 раз).
        int count2 = 0;
        do {
            System.out.println(count2);
            count2++;
        } while (count2 < 5);

//        2. Переход на следующую итерацию цикла (continue) и прерывание выполнения цикла (break).
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (j == 5) {
                    continue;                            //переход на следующую итерацию самого внутреннего цикла (по j).
                }
                if (j == 7) {
                    break;                              //прерывание самого внутреннего цикла (по j).
                }
                System.out.println("i=" + i + " j=" + j);
            }
        }

        AAA: for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (i == 5) {
                    continue AAA;                       //переход на следующую итерацию цикла, помеченного лейблом AAA.
                }
                if (i == 7) {
                    break AAA;                          //прерывание цикла, помеченного лейблом AAA.
                }
                System.out.println("i=" + i + " j=" + j);
            }
        }
    }
}
